package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class PalindromeTest {



	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome",
		Palindrome.isPalindrome("race car"));	
	}
	
	@Test
	public void testPlaindromeIn() {
		assertTrue("Invalid value for palindrome",
		Palindrome.isPalindrome("A"));	
	}
	
	@Test
	public void testPalindromeNegative() {
		assertFalse("Invalid value for palindrome",
		Palindrome.isPalindrome("hello"));	
	}
	
	@Test
	public void testPalindromeoutBoundryOut() {
		assertFalse("Invalid value for palindrome",
		Palindrome.isPalindrome("racelcar"));	
	}

	

}
